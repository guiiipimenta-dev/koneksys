import { Response } from "express";
import { ListEquipmentsService } from "./ListEquipmentsService";

export class ListEquipmentsController {
  constructor(private listEquipmentService: ListEquipmentsService) {}

  async handle(res: Response): Promise<Response> {
    const result = await this.listEquipmentService.execute();

    if (result instanceof Error) {
      return res.status(400).json(result.message);
    }
    return res.status(200).send(result);
  }
}
