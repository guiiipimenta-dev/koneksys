import { Request, Response } from "express";
import { UpdateManufacturerService } from "./UpdateManufacturerService";

export class UpdateManufacturerController {
  constructor(private createManufacturerService: UpdateManufacturerService) {}

  async handle(req: Request, res: Response): Promise<Response> {
    const { id } = req.params;
    const { name } = req.body;

    const result = await this.createManufacturerService.execute({
      id,
      name,
    });
    if (result instanceof Error) {
      return res.status(400).json(result.message);
    }

    return res.status(200).send(result);
  }
}
