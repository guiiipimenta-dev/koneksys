"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateManufacturerController = void 0;
class UpdateManufacturerController {
    constructor(createManufacturerService) {
        this.createManufacturerService = createManufacturerService;
    }
    async handle(req, res) {
        const { id } = req.params;
        const { name } = req.body;
        const result = await this.createManufacturerService.execute({
            id,
            name,
        });
        if (result instanceof Error) {
            return res.status(400).json(result.message);
        }
        return res.status(200).send(result);
    }
}
exports.UpdateManufacturerController = UpdateManufacturerController;
//# sourceMappingURL=UpdateManufacturerController.js.map