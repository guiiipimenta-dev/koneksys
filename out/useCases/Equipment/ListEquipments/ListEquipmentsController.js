"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ListEquipmentsController = void 0;
class ListEquipmentsController {
    constructor(listEquipmentService) {
        this.listEquipmentService = listEquipmentService;
    }
    async handle(res) {
        const result = await this.listEquipmentService.execute();
        if (result instanceof Error) {
            return res.status(400).json(result.message);
        }
        return res.status(200).send(result);
    }
}
exports.ListEquipmentsController = ListEquipmentsController;
//# sourceMappingURL=ListEquipmentsController.js.map