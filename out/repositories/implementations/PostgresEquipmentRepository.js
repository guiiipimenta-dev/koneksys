"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PostgresEquipmentsRepository = void 0;
const fc_1 = require("../../utils/fc");
const { Client } = require("pg");
class PostgresEquipmentsRepository {
    constructor(client) {
        this.db = client;
    }
    async findById(id) {
        const { rows } = await this.db.query(fc_1.readQuery("selectEquipment"), [id]);
        return rows[0];
    }
    async listOwner(id) {
        const { rows } = await this.db.query(fc_1.readQuery("selectOwner"), [id]);
        return rows[0];
    }
    async save(equipment) {
        await this.db.query(fc_1.readQuery("insertEquipment"), [
            equipment.id,
            equipment.model,
            equipment.serialNumber,
            equipment.manufacturerId,
        ]);
        return equipment;
    }
    async listAll() {
        const { rows } = await this.db.query(fc_1.readQuery("selectEquipments"));
        return rows;
    }
    async listOne(id) {
        const { rows } = await this.db.query(fc_1.readQuery("selectEquipment"), [id]);
        return rows[0];
    }
    async update(dto) {
        const { model, serialNumber } = await this.listOne(dto.id);
        dto.model = dto.model ? dto.model : model;
        dto.serialNumber = dto.serialNumber ? dto.serialNumber : serialNumber;
        dto.manufacturerId = dto.manufacturerId;
        await this.db.query(fc_1.readQuery("updateEquipment"), [
            dto.model,
            dto.serialNumber,
            dto.manufacturerId,
            dto.id,
        ]);
        return dto;
    }
    async delete(id) {
        await this.db.query(fc_1.readQuery("deleteEquipment"), [id]);
    }
}
exports.PostgresEquipmentsRepository = PostgresEquipmentsRepository;
//# sourceMappingURL=PostgresEquipmentRepository.js.map