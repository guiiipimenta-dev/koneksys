const { Client } = require("pg");
require("dotenv/config");
const client = new Client({
    user: process.env.DB_USER,
    host: process.env.DB_HOST,
    database: process.env.DB_NAME,
    password: process.env.TEST_DB_PASS,
    port: process.env.TEST_DB_PORT,
});
client.connect();
module.exports = client;
//# sourceMappingURL=index-test.js.map